/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.teresafernandes.minhaarquitetura.exception;

/**
 *
 * @author Teresa
 */
public class NegocioException extends Exception{

    private static final long serialVersionUID = 1L;

    public NegocioException(String message) {
        super(message);
    }
}
