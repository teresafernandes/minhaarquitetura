package com.github.teresafernandes.minhaarquitetura.util;

import java.util.List;

import com.github.teresafernandes.minhaarquitetura.dominio.EntidadeId;
import com.github.teresafernandes.minhaarquitetura.exception.ValidationException;
import com.github.teresafernandes.minhaarquitetura.validator.Cep;
import com.github.teresafernandes.minhaarquitetura.validator.Cnpj;
import com.github.teresafernandes.minhaarquitetura.validator.Cpf;
import com.github.teresafernandes.minhaarquitetura.validator.CpfOuCnpj;
import com.github.teresafernandes.minhaarquitetura.validator.Rg;

public class ValidatorUtil {

	public static void cep(String value) throws ValidationException{
		Cep.isValid(value);
	}
	
	public static void cnpj(String value) throws ValidationException{
		Cnpj.isValid(value);
	}
	
	public static void cpf(String value) throws ValidationException{
		Cpf.isValid(value);
	}
	
	public static void cpfOuCnpj(String value) throws ValidationException{
		CpfOuCnpj.isValid(value);
	}
	
	public static void rg(String value) throws ValidationException{
		Rg.isValid(value);
	}
	
	
	public static void email(String value, String message){
		
	}
	
	public static void validate(String campo, Object value) throws ValidationException{
		if(ValidatorUtil.isEmpty(value))
			throw new ValidationException(campo+": Campo obrigatório não informado.");
	}
	
	public static void validate(String campo, Object value, List<String> erros){
		try {
			validate(campo, value);
		} catch (ValidationException e) {
			erros.add(e.getMessage());
		}
	}
	
	
	public static boolean isNotEmpty(Object value) {
		return !isEmpty(value);
	}
	
	public static boolean isEmpty(Object value) {
		if(value == null)
			return true;
		else if(value instanceof String && ((String) value).isEmpty())
			return true;
		else if (value instanceof EntidadeId){
			Object obj = ReflectionUtil.executeMethod(value, "getId");
			 if(obj == null || ((Integer) obj).compareTo(0) <= 0)
				 return true;
		}
		return false;
	}

}
