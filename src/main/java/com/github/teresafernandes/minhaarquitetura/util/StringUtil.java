package com.github.teresafernandes.minhaarquitetura.util;

import java.util.List;

public class StringUtil {

	public static String listaToString(List<String> lista){
		String resultado = "";
		if(lista != null){
			for(String s : lista){
				resultado += s + "<br>";
			}
		}
		return resultado;
	}

	public static void parseToString(String label, String value, List<String> tostring){
		if(ValidatorUtil.isNotEmpty(value)) tostring.add("<b> "+label+": </b>"+value);
	}
}
