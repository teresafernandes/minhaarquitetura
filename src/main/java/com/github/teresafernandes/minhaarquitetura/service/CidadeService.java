package com.github.teresafernandes.minhaarquitetura.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.teresafernandes.minhaarquitetura.dominio.Cidade;
import com.github.teresafernandes.minhaarquitetura.repository.CidadeRepository;

@Service
public class CidadeService {

	@Autowired
	private CidadeRepository cidadeRepository;
	
	public List<Cidade> findAll(){
		return cidadeRepository.findAll();
	}
	
	public Cidade findById(final Integer id){
		return cidadeRepository.findById(id);
	}
	
	public List<Cidade> findByEstadoId(final Integer id){
		return (List<Cidade>) cidadeRepository.findByEstadoId(id);
	}
}
