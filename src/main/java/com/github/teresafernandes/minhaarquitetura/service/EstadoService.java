package com.github.teresafernandes.minhaarquitetura.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.teresafernandes.minhaarquitetura.dominio.Estado;
import com.github.teresafernandes.minhaarquitetura.repository.EstadoRepository;

@Service
public class EstadoService {

	@Autowired
	private EstadoRepository estadoRepository;
	
	public List<Estado> findAll(){
		return estadoRepository.findAll();
	}
	
	public Estado findById(final Integer id){
		return estadoRepository.findById(id);
	}
	
}
