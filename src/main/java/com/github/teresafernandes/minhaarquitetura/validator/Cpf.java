/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.teresafernandes.minhaarquitetura.validator;

import com.github.teresafernandes.minhaarquitetura.exception.ValidationException;


/**
 *
 * @author Teresa
 */
public class Cpf {

    @SuppressWarnings("null")
	public static void isValid(String value) throws ValidationException{
        if (value != null || value.compareTo("")==0) {
            value = value.toString().replaceAll("[- /.]", "");
            value= value.toString().replaceAll("[-()]", "");
            
           if(!validaCPF(value)){
        	   throw new ValidationException("Cpf inválido.");
           }
        }
    }

    public static boolean validaCPF(String cpfNum) {
        int[] cpf = new int[cpfNum.length()]; //define o valor com o tamanho da string  
        int resultP = 0;
        int resultS = 0;

        //converte a string para um array de integer  
        for (int i = 0; i < cpf.length; i++) {
            cpf[i] = Integer.parseInt(cpfNum.substring(i, i + 1));
        }

        //calcula o primeiro número(DIV) do cpf  
        for (int i = 0; i < 9; i++) {
            resultP += cpf[i] * (i + 1);
        }
        int divP = resultP % 11;

        //se o resultado for diferente ao 10º digito do cpf retorna falso  
        if (divP != cpf[9]) {
            return false;
        } else {
            //calcula o segundo número(DIV) do cpf  
            for (int i = 0; i < 10; i++) {
                resultS += cpf[i] * (i);
            }
            int divS = resultS % 11;

            //se o resultado for diferente ao 11º digito do cpf retorna falso  
            if (divS != cpf[10]) {
                return false;
            }
        }

        //se tudo estiver ok retorna verdadeiro  
        return true;
    }//fim do calcular cpf ==================================================  
}
