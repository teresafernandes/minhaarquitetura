/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.teresafernandes.minhaarquitetura.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.teresafernandes.minhaarquitetura.exception.ValidationException;

/**
 *
 * @author Teresa
 */
public class Cep  {

    

    public static void isValid(String value) throws ValidationException {
    
    	Pattern pattern = Pattern.compile("[0-9]{5}-[0-9]{3}");
        Matcher m = pattern.matcher(value);
        
        if(!m.matches()){
        	throw new ValidationException("Cep inválido");
        }
    }
}
