/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.teresafernandes.minhaarquitetura.validator;

import com.github.teresafernandes.minhaarquitetura.exception.ValidationException;


/**
 *
 * @author Teresa
 */
public class Cnpj {
    
    public static void isValid(String value) throws ValidationException {
        if (value != null || value != "") {
            value = value.toString().replaceAll("[- /.]", "");
            value= value.toString().replaceAll("[-()]", "");
            
            if(!validaCNPJ(value)){
            	throw new ValidationException("Cnpj inválido.");
            }
        }        
    }
    
    public static boolean validaCNPJ(String cnpjNum) {
        int[] cnpj = new int[cnpjNum.length()];
        int resultP = 0;
        int resultS = 0;
        int divP = 0;
        int divS = 0;

        //converte string para um array de integer  
        for (int i = 0; i < cnpjNum.length(); i++) {
            cnpj[i] = Integer.parseInt(cnpjNum.substring(i, i + 1));
        }

        int j = 6;
        //calcula o primeiro div  
        for (int i = 0; i < 12; i++) {
            resultP += cnpj[i] * j;

            j++;
            if (j > 9) {
                j = 2;
            }
        }
        divP = resultP % 11;

        if (divP != cnpj[12]) {
            return false;
        } else {
            j = 5;
            //calcula o segundo div   
            for (int i = 0; i < 13; i++) {
                resultS += cnpj[i] * j;

                j++;
                if (j > 9) {
                    j = 2;
                }
            }
            divS = resultS % 11;

            if (divS != cnpj[13]) {
                return false;
            }
        }
        return true;
    }//fim do calcular CNPJ ==================================================  
}
