/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.teresafernandes.minhaarquitetura.validator;


/**
 *
 * @author Teresa
 */
public class DoubleGreaterThanZero {
    
	public boolean isValid(String value) {

        if (value.length() == 0) {
            return false;
        }

        Double d = Double.parseDouble(value);
        if (d == null || d <= 0) {
            return false;
        }
        
        return true;
    }
}
