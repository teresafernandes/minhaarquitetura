/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.teresafernandes.minhaarquitetura.validator;


/**
 *
 * @author Teresa
 */
public class IntegerGreaterThanZero  {

    public boolean isValid(String value) {

        if (value.length() == 0) {
            return false;
        }

        Integer i = Integer.parseInt(value);
        if (i == null || i <= 0) {
            return false;
        }
        
        return true;
    }
}
