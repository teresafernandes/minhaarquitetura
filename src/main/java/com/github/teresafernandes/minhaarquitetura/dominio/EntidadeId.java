package com.github.teresafernandes.minhaarquitetura.dominio;

public interface  EntidadeId {

	public Integer getId();
	public void setId(Integer id);
}
