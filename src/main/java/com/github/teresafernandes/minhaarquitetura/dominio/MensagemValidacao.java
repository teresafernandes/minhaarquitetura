package com.github.teresafernandes.minhaarquitetura.dominio;

import javax.persistence.Transient;


public class MensagemValidacao {
	@Transient
	String mensagem;

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
