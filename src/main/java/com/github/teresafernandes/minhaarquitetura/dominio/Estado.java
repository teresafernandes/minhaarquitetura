package com.github.teresafernandes.minhaarquitetura.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Estado implements EntidadeId{

    @Id
    @Column(name="id_estado")
    @GeneratedValue
    private Integer id;
    
    private String nome;
 
    private String sigla;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
    
    public Estado(){}
    
    public Estado(String nome, String sigla){
    	this.nome = nome;
    	this.sigla = sigla;
    }
}
