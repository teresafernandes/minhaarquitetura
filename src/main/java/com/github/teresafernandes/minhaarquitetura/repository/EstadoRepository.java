package com.github.teresafernandes.minhaarquitetura.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.github.teresafernandes.minhaarquitetura.dominio.Estado;


public interface EstadoRepository extends JpaRepository<Estado, Long> {
	Estado findBySigla(String sigla);
	Estado findById(Integer estadoId);
}
