package com.github.teresafernandes.minhaarquitetura.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.github.teresafernandes.minhaarquitetura.dominio.Cidade;

@Repository
public interface CidadeRepository  extends JpaRepository<Cidade, Long> {
	Collection<Cidade> findByUf(String uf);
	Collection<Cidade> findByEstadoId(Integer estadoId);
	Cidade findById(Integer cidadeId);
}
